<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width", initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>
    <div class="container-full">
        <div class="col-md-12 top_head">
            Enrollment in progress throughout the year. Intake for 8-4-4 Form 1 and 2, and IGCSE years 9 and 10 are on going. Prospective parents and
            students are encouraged to visit the school, meet the staff and take a look at the modern facilities.
        </div>
        <div class="col-md-12 nav_top">
            <ul>
                <li><a href="#">Home</a></li>
                <li>|</li>
                <li><a href="#">About</a></li>
                <li>|</li>
                <li><a href="#">Exams</a></li>
                <li>|</li>
                <li><a href="#">Life at Lorem High</a></li>
                <li>|</li>
                <li><a href="#">Gallery</a></li>
            </ul>
        </div>
        <div class="col-md-2 logo">
            <img src="img/bristar-logo.gif">
        </div>
        <div class="col-md-10">
            <div class="row nav_inside">
                <div class="col-md-3 portion">
                    <img src="img/school_one.jpg" class="nav_img">
                    <div class="col-md-12 nav_inside_text"><a href="#">Admissions</a></div>
                </div>
                <div class="col-md-3 portion">
                    <img src="img/school_two.jpg" class="nav_img">
                    <div class="col-md-12 nav_inside_text"><a href="#">Calenders</a></div>
                </div>
                <div class="col-md-3 portion">
                    <img src="img/school_four.jpg" class="nav_img">
                    <div class="col-md-12 nav_inside_text"><a href="#">News and Events</a></div>
                </div>
                <div class="col-md-3 portion">
                    <img src="img/school_three.jpg" class="nav_img">
                    <div class="col-md-12 nav_inside_text"><a href="#">Contacts</a></div>
                </div>
            </div>

        </div>
        <div class="col-md-12 img_large">

            <div class="row col-md-offset-6 info">
                 <p>December 15 2015</p>
                 <p class="text_bold">Prize Giving</p>
                 <p>Please clear school fess by end of the day </p>
                 <br>
                 <br>
                 <p>December 15 2015</p>
                 <p class="text_bold">Teachers Retreat to Malaysia</p>
                 <p>Be prepared to continue to prepared to visit Malaysia</p>
                 <p>this holiday 14th April to 22nd April 2016.Geer up people</p>
                 <p>it has never been this good.</p>
                 <a href="#">More news and Events </a>
            </div>
        </div>
        <div class="row col-md-12">
            <div class="heading">
                <h4>Welcome to Lorem High School</h4>
            </div>
            <div class="col-md-offset-3">

                <div class="row col-md-5 data">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
                </div>

                <div class="row col-md-5 data">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
                </div>

            </div>
        </div>
        <div class="col-md-12 footer">

            <div class="col-md-3">
                <img src="img/bristar-logo.gif">
            </div>
            <div class="col-md-3">
                <h3>ABOUT US</h3>
                <br>

                <p><a href="#">Who we are</a></p>
                <p><a href="#">Why it matters</a></p>
                <p><a href="#">The Governance</a></p>

                <div class="col-md-offset-10 vertical-line"></div>

            </div>

            <div class="col-md-3">
                <h3>ADMISSIONS</h3>
                <br>

                <p><a href="#">Requirements</a></p>
                <p><a href="#">Applying</a></p>
                <p><a href="#">Resources</a></p>

                <div class="col-md-offset-10 vertical-line"></div>

            </div>
            <div class="col-md-3">
                <h3>STUDENT LIFE</h3>
                <br>

                <p><a href="#">Quality of life</a></p>
                <p><a href="#">Life together</a></p>
                <p><a href="#">Our legacy</a></p>

            </div>

            <div class="row col-md-offset-6 col-md-6 copy"><a href="#">&copy; 2014 Lorem High School</a>, <a href="#">P.O.BOX 123 Nairobi Kenya</a>, <a href="#">Terms & conditions</a> | <a href="#">Privacy Policy</a></div>
            <p class="col-md-offset-10 para"><a href="#">Handcrafted in Kenya</a></p>


        </div>
    </div>
</body>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</html>